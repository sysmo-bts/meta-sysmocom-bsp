# OS#4062, OS#5079: When building with sysmobts toolchain we hit a TLS compiler
# bug (osmo-bts-symo calling osmo_str_tolower() which uses "static __thread char
# capsbuf"). As a result, the only option would be to use -O0 when building
# libosmocore. In order to avoid that, we take advantage of the fact that no
# libosmocore multi-thread process is expected to run there and simply drop all
# the "__thread" references when building for sysmobts. That's done as a
# machine-specific bbappend in meta-sysmocom-bsp.
do_compile_prepend() {
        grep -r -l "__thread" "${S}/src/" "${S}/include/" | xargs sed -i "s/__thread//g"
}
