DESCRIPTION = "sysmocom OsmoBTS"
LICENSE = "AGPLv3"
LIC_FILES_CHKSUM = "file://COPYING;md5=73f1eb20517c55bf9493b7dd6e480788"

SRCREV = "1.9.0"
SRC_URI = "git://gerrit.osmocom.org/osmo-bts;protocol=https;destsuffix=git;tag=${SRCREV};nobranch=1"
PV = "${SRCREV}+git${SRCPV}"
PR = "r0.${META_TELEPHONY_OSMO_INC}"
S = "${WORKDIR}/git"

DEPENDS = "libosmocore libosmo-abis libosmo-netif femtobts-api gpsd"
DEPENDS_append_sysmobts-v2 = " femtobts-api"
DEPENDS_append_sysmobts2100 = " lc15-firmware"
DEPENDS_append_oc2g = " oc2g-firmware systemd"

RDEPENDS_${PN} += "coreutils"

RDEPENDS_${PN}_append_sysmobts-v2 = " sysmobts-firmware (>= 5.1)"
RCONFLICTS_${PN}_append_sysmobts-v2 = " sysmobts-firmware (< 5.1)"

RDEPENDS_${PN}_append_sysmobts2100 = " lc15-firmware"
RDEPENDS_${PN}_append_oc2g = " oc2g-firmware systemd"

EXTRA_OECONF_sysmobts-v2 += "--enable-sysmocom-bts --enable-sysmobts-calib"
EXTRA_OECONF_sysmobts2100 += "--enable-litecell15"
EXTRA_OECONF_oc2g += "--enable-oc2g"

inherit autotools pkgconfig systemd

PACKAGECONFIG ??= "\
    ${@bb.utils.filter('DISTRO_FEATURES', 'systemd', d)} \
    "
PACKAGECONFIG[systemd] = "--with-systemdsystemunitdir=${systemd_system_unitdir},--without-systemdsystemunitdir"

# StateDirectory requires systemd >= v235, but poky pyro ships with v232 (SYS#6340):
do_install_append_sysmobts-v2() {
	sed -i '/^StateDirectory=/d' ${D}${systemd_system_unitdir}/osmo-bts-sysmo.service
	sed -i '/^WorkingDirectory=/d' ${D}${systemd_system_unitdir}/osmo-bts-sysmo.service
	sed -i '/^StateDirectory=/d' ${D}${systemd_system_unitdir}/sysmobts-mgr.service
	sed -i '/^WorkingDirectory=/d' ${D}${systemd_system_unitdir}/sysmobts-mgr.service
}
do_install_append_sysmobts2100() {
	sed -i '/^StateDirectory=/d' ${D}${systemd_system_unitdir}/osmo-bts-lc15.service
	sed -i '/^WorkingDirectory=/d' ${D}${systemd_system_unitdir}/osmo-bts-lc15.service
	sed -i '/^StateDirectory=/d' ${D}${systemd_system_unitdir}/lc15bts-mgr.service
	sed -i '/^WorkingDirectory=/d' ${D}${systemd_system_unitdir}/lc15bts-mgr.service
}
do_install_append_oc2g() {
	sed -i '/^StateDirectory=/d' ${D}${systemd_system_unitdir}/osmo-bts-oc2g.service
	sed -i '/^WorkingDirectory=/d' ${D}${systemd_system_unitdir}/osmo-bts-oc2g.service
	sed -i '/^StateDirectory=/d' ${D}${systemd_system_unitdir}/oc2gbts-mgr.service
	sed -i '/^WorkingDirectory=/d' ${D}${systemd_system_unitdir}/oc2gbts-mgr.service
}

SYSTEMD_PACKAGES = "${PN} osmo-bts-virtual"
SYSTEMD_AUTO_ENABLE_${PN}="enable"

# Select the API version
inherit femtobts_api
CPPFLAGS_append_sysmobts-v2 = " ${BTS_HW_VERSION} "

SYSTEMD_SERVICE_${PN}_append_sysmobts-v2 = " sysmobts-mgr.service osmo-bts-sysmo.service"
SYSTEMD_SERVICE_${PN}_append_sysmobts2100 = " lc15bts-mgr.service osmo-bts-lc15.service"
SYSTEMD_SERVICE_${PN}_append_oc2g = " oc2gbts-mgr.service osmo-bts-oc2g.service"

CONFFILES_${PN}_append_sysmobts-v2 = " ${sysconfdir}/osmocom/sysmobts-mgr.cfg ${sysconfdir}/osmocom/osmo-bts-sysmo.cfg"
CONFFILES_${PN}_append_sysmobts2100 = " ${sysconfdir}/osmocom/lc15bts-mgr.cfg ${sysconfdir}/osmocom/osmo-bts-lc15.cfg"
CONFFILES_${PN}_append_oc2g = " ${sysconfdir}/oc2gbts-mgr.cfg ${sysconfdir}/osmocom/osmo-bts-oc2g.cfg"

# somehow it seems not posible to use _append constructs on PACKAGES
#PACKAGES_append_sysmobts-v2 = " osmo-bts-remote sysmobts-calib sysmobts-util"
PACKAGES =+ "osmo-bts-remote sysmobts-calib sysmobts-util osmo-bts-virtual"

FILES_osmo-bts-remote_sysmobts-v2 = " ${bindir}/osmo-bts-sysmo-remote "
FILES_sysmobts-calib_sysmobts-v2 = " ${bindir}/sysmobts-calib "
FILES_sysmobts-util = " ${bindir}/sysmobts-util "

SYSTEMD_SERVICE_osmo-bts-virtual = "osmo-bts-virtual.service"
FILES_osmo-bts-virtual = " \
		${docdir}/osmo-sgsn/examples/osmo-bts-virtual \
		${bindir}/osmo-bts-virtual \
		${bindir}/osmo-bts-omldummy \
		"
CONFFILES_osmo-bts-virtual = "\
		${sysconfdir}/osmocom/osmo-bts-virtual.cfg \
		"
