DESCRIPTION = "Osmocom PCU for sysmoBTS"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=b234ee4d69f5fce4486a80fdaf4a4263"

SRCREV = "1.5.1"
SRC_URI = "git://gerrit.osmocom.org/osmo-pcu;protocol=https;branch=master;nobranch=1"
PV = "${SRCREV}+git${SRCPV}"
PR = "r1.${META_TELEPHONY_OSMO_INC}"
S = "${WORKDIR}/git"

DEPENDS = "libosmocore osmo-bts"
DEPENDS_append_sysmobts-v2 = " femtobts-api"
DEPENDS_append_litecell15   = " lc15-firmware"
DEPENDS_append_sysmobts2100 = " lc15-firmware"
DEPENDS_append_oc2g = " oc2g-firmware"

# This implements PCU Interface v8 (GPRS RSSI)
RDEPENDS_${PN} = "osmo-bts (>= 0.8.0)"

EXTRA_OECONF_sysmobts-v2 += "--enable-sysmocom-dsp"
EXTRA_OECONF_litecell15   += "--enable-lc15bts-phy"
EXTRA_OECONF_sysmobts2100 += "--enable-lc15bts-phy"
EXTRA_OECONF_oc2g += "--enable-oc2gbts-phy"

inherit autotools pkgconfig systemd

PACKAGECONFIG ??= "\
    ${@bb.utils.filter('DISTRO_FEATURES', 'systemd', d)} \
    "
PACKAGECONFIG[systemd] = "--with-systemdsystemunitdir=${systemd_system_unitdir},--without-systemdsystemunitdir"

do_install_append() {
	# StateDirectory requires systemd >= v235, but poky pyro ships with v232 (SYS#6340):
	sed -i '/^StateDirectory=/d' ${D}${systemd_system_unitdir}/osmo-pcu.service
	sed -i '/^WorkingDirectory=/d' ${D}${systemd_system_unitdir}/osmo-pcu.service

	# Run as root (OS#5684):
	sed -i '/^User=/d' "${D}${systemd_system_unitdir}/osmo-pcu.service"
	sed -i '/^Group=/d' "${D}${systemd_system_unitdir}/osmo-pcu.service"
}

SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_AUTO_ENABLE_${PN}="enable"
SYSTEMD_SERVICE_${PN} = "osmo-pcu.service"

# Select the API version
inherit femtobts_api
CPPFLAGS_append_sysmobts-v2 = " ${BTS_HW_VERSION} "

CONFFILES_${PN} = "${sysconfdir}/osmocom/osmo-pcu.cfg"
