# Not provided in our OE packages
DEPENDS_remove = "virtual/crypt"

# Install sftp-server so scp works with the SFTP protocol, which is the default
# in recent OpenSSH versions (SYS#6403)
RDEPENDS_${PN}_append = " sftp-server"

do_install_append() {
    # Do not disable root login by default
    echo "" > ${D}${sysconfdir}/default/dropbear
}
