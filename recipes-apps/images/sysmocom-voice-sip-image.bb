require sysmocom-image.inc

IMAGE_INSTALL_append = " task-sysmocom-cs osmo-sip-connector "

# osmo-pcu is included in sysmobts targets due to machine .conf depending on
# task-sysmocom-bts. Since we only want voice here, let's disable osmo-pcu and
# be done with it:
disable_systemd_osmo_pcu() {
        rm -f ${IMAGE_ROOTFS}/etc/systemd/system/multi-user.target.wants/osmo-pcu.service
}
IMAGE_PREPROCESS_COMMAND += "disable_systemd_osmo_pcu "
