HOMEPAGE = "http://www.sysmocom.de"
RDEPENDS_${PN} = "openvpn"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

SRC_URI = "file://sysmocom-vpn.conf"
PR = "r7"

CONFFILES_${PN} = "${sysconfdir}/openvpn/sysmocom-vpn.conf"
PACKAGE_ARCH = "all"

do_install() {
	install -d ${D}${sysconfdir}/openvpn
	install -m 0644 ${WORKDIR}/sysmocom-vpn.conf ${D}${sysconfdir}/openvpn
}
