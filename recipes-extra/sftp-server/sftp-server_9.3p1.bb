# Package OpenSSH's sftp server, so the openssh client finds it and doesn't
# need to be run with a flag to use the legacy SCP protocol (SYS#6403)
SUMMARY = "OpenSSH's sftp-server"
DESCRIPTION = "OpenSSH's sftp-server, to be used with dropbear"
HOMEPAGE = "http://www.openssh.com/"
SECTION = "console/network"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENCE;md5=072979064e691d342002f43cd89c0394"
DEPENDS = ""
SRC_URI = "https://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-${PV}.tar.gz"

SRC_URI[md5sum] = "3430d5e6e71419e28f440a42563cb553"
SRC_URI[sha256sum] = "e9baba7701a76a51f3d85a62c383a3c9dcd97fa900b859bc7db114c1868af8a8"

inherit autotools

EXTRA_OECONF += "--without-openssl --without-zlib"

S = "${WORKDIR}/openssh-${PV}"

do_compile() {
    oe_runmake sftp-server
}

do_install() {
    install -Dm755 "${B}"/sftp-server \
        -t "${D}"/usr/libexec/
}

FILES_${PN} = "/usr/libexec/sftp-server"
